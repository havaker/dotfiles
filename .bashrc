# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

[[ $- != *i* ]] && return

export PS1="\n\[$(tput bold)\]\[$(tput sgr0)\]\[\033[38;5;9m\]\u\[$(tput sgr0)\]\[\033[38;5;238m\]@\[$(tput sgr0)\]\[$(tput sgr0)\]\[\033[38;5;230m\]\h\[$(tput sgr0)\]\[\033[38;5;32m\][\[$(tput sgr0)\]\[\033[38;5;36m\]\w\[$(tput sgr0)\]\[\033[38;5;32m\]]\[$(tput sgr0)\]\[\033[38;5;15m\] \t\n>\[$(tput sgr0)\]"

alias ccr='~/Scripts/ccr.rb'
alias pro='cd ~/Programming'
alias prg='cd ~/Programming'
alias dow='cd ~/Downloads'
alias doc='cd ~/Documents'
alias odro='ssh c2'
alias vim='vimx'
alias ls='ls -lh --color'
alias la='ls -lah --color'
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
alias szef='based-connect $( ~/Scripts/szef_address.py ) '

bind 'set completion-ignore-case on'

eval $(thefuck --alias)

function extract {
 if [ -z "$1" ]; then
    # display usage if no parameters given
    echo "Usage: extract <path/file_name>.<zip|rar|bz2|gz|tar|tbz2|tgz|Z|7z|xz|ex|tar.bz2|tar.gz|tar.xz>"
    echo "       extract <path/file_name_1.ext> [path/file_name_2.ext] [path/file_name_3.ext]"
    return 1
 else
    for n in $@
    do
      if [ -f "$n" ] ; then
          case "${n%,}" in
            *.tar.bz2|*.tar.gz|*.tar.xz|*.tbz2|*.tgz|*.txz|*.tar)
                         tar xvf "$n"       ;;
            *.lzma)      unlzma ./"$n"      ;;
            *.bz2)       bunzip2 ./"$n"     ;;
            *.rar)       unrar x -ad ./"$n" ;;
            *.gz)        gunzip ./"$n"      ;;
            *.zip)       unzip ./"$n"       ;;
            *.z)         uncompress ./"$n"  ;;
            *.7z|*.arj|*.cab|*.chm|*.deb|*.dmg|*.iso|*.lzh|*.msi|*.rpm|*.udf|*.wim|*.xar)
                         7z x ./"$n"        ;;
            *.xz)        unxz ./"$n"        ;;
            *.exe)       cabextract ./"$n"  ;;
            *)
                         echo "extract: '$n' - unknown archive method"
                         return 1
                         ;;
          esac
      else
          echo "'$n' - file does not exist"
          return 1
      fi
    done
fi
}

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
export CARGO_HOME="$HOME/.cargo"

export PATH="$HOME/.cargo/bin:$PATH"
export PATH="/home/havaker/.local/share/flatpak/app/com.google.AndroidStudio/x86_64/stable/d953599d5ec499d1378b56c54eace3b19d08d2405f2ea67542788857fbb2ba8d/files/extra/jre/bin/:$PATH"
export PATH="/home/havaker/.local/share/flatpak/app/com.google.AndroidStudio/x86_64/stable/d953599d5ec499d1378b56c54eace3b19d08d2405f2ea67542788857fbb2ba8d/files/extra/gradle/gradle-4.4/bin/:$PATH"
export ANDROID_HOME=/home/havaker/Android/Sdk
export PATH=${PATH}:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools

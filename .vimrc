set exrc
set secure
set encoding=utf-8
set nocompatible              " be iMproved, required
filetype off                  " required

set mouse=a

"set shiftwidth=2
"set tabstop=2
set tabstop=2 shiftwidth=2 expandtab
set autoindent
set smartindent
set linebreak


set pastetoggle=<F2>

" pls work backspace
set backspace=indent,eol,start

vnoremap <C-c> "*y :let @+=@*<CR>

set so=20

noremap  <buffer> <silent> k gk
noremap  <buffer> <silent> j gj
noremap  <buffer> <silent> 0 g0
noremap  <buffer> <silent> $ g$

"easier split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

imap jk <Esc>
nmap ; :


syntax on
set background=dark
colorscheme oxeded
let g:airline_theme='bubblegum'
set number



set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.o,*.d
let NERDTreeIgnore = ['\.o$', '\.d$']
map <C-n> :NERDTreeToggle<CR>
let g:ycm_confirm_extra_conf = 0
vnoremap <C-d> :YcmCompleter GoToDefinition<CR>
vnoremap <C-r> :YcmCompleter GoToReferences<CR>
let g:vim_redraw = 1
let g:ctrlp_custom_ignore = 'node_modules\|DS_Store\|git'
let g:ctrlp_working_path_mode = 0



"try to show as much as possible of the last line in the window (rather than a column of "@", which is the default behavior)
set display+=lastline

"The following provides the maps gH, gM, and gL to emulate H, M, and L even when very long lines are present.
nnoremap <silent> <expr> gH winline() - 1 - &scrolloff > 0
      \ ? ':normal! ' . (winline() - 1 - &scrolloff) . 'gkg^<CR>'
      \ : 'g^'
nnoremap <silent> <expr> gM winline() < (winheight(0)+1)/2
      \ ? ':normal! ' . ((winheight(0)+1)/2 - winline()) . 'gjg^<CR>'
      \ : winline() == (winheight(0)+1)/2
      \         ? 'g^'
      \         : ':normal! ' . (winline() - (winheight(0)+1)/2) . 'gkg^<CR>'
nnoremap <silent> <expr> gL winheight(0) - winline() - &scrolloff > 0
      \ ? ':normal! ' . (winheight(0) - winline() - &scrolloff) . 'gjg^<CR>'
      \ : 'g^'


"copying to clipboard
vnoremap <C-c> "+y
"vnoremap <C-c> "*y :let @+=@*<CR>
map <C-v> "+P


" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'valloric/youcompleteme'
Plugin 'rust-lang/rust.vim'
Plugin 'vim-syntastic/syntastic'
Plugin 'rust-lang-nursery/rustfmt'
Plugin 'octol/vim-cpp-enhanced-highlight'
Plugin 'scrooloose/nerdtree'
Plugin 'junegunn/fzf.vim'
Plugin 'junegunn/fzf'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'vim-scripts/Conque-GDB'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'dermusikman/sonicpi.vim'
Plugin 'jeaye/color_coded'
Plugin 'flazz/vim-colorschemes'
Plugin 'tomlion/vim-solidity'
Plugin 'terryma/vim-expand-region'
Plugin 'eugen0329/vim-esearch'
Plugin 'leafgarland/typescript-vim'
Plugin 'jpalardy/vim-slime'
Plugin 'OCamlPro/ocp-indent'
Plugin 'posva/vim-vue'
Plugin 'editorconfig/editorconfig-vim'



" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" Vue syntax highligting
autocmd BufNewFile,BufRead *.vue set ft=vue
au BufRead,BufNewFile *.rs set filetype=rust
autocmd BufNewFile,BufRead *.ts setlocal filetype=typescript

let s:hidden_all = 0
function! ToggleHiddenAll()
    if s:hidden_all  == 0
        let s:hidden_all = 1
        set noshowmode
        set noruler
"        set laststatus=0
        set noshowcmd
    else
        let s:hidden_all = 0
        set showmode
        set ruler
"        set laststatus=2
        set showcmd
    endif
endfunction

nnoremap <C-m> :call ToggleHiddenAll()<CR>

set hls
"pressing F8 will highlight all occurrences of the current word
set guioptions+=a
function! MakePattern(text)
  let pat = escape(a:text, '\')
  let pat = substitute(pat, '\_s\+$', '\\s\\*', '')
  let pat = substitute(pat, '^\_s\+', '\\s\\*', '')
  let pat = substitute(pat, '\_s\+',  '\\_s\\+', 'g')
  return '\\V' . escape(pat, '\"')
endfunction
vnoremap <silent> <F8> :<C-U>let @/="<C-R>=MakePattern(@*)<CR>"<CR>:set hls<CR>

fun! Download_and_change_scheme( arg ) "{{{
	silent execute "!~/Scripts/download_colorscheme.rb " . a:arg . " r "
	let l:newcs = readfile( "/tmp/newcolorscheme" )[0]
	execute 'colorscheme' l:newcs
endfunction "}}}

command! -nargs=* AddScheme call Download_and_change_scheme( '<args>' )

" autocmd FileType ocaml let g:loaded_youcompleteme = 1 
autocmd FileType ocaml let g:ycm_filetype_blacklist = {
      \ 'ocaml' : 1,
      \ 'qf' : 1,
      \ 'notes' : 1,
      \ 'markdown' : 1,
      \ 'unite' : 1,
      \ 'text' : 1,
      \ 'vimwiki' : 1,
      \ 'pandoc' : 1,
      \ 'infolog' : 1,
      \ 'mail' : 1
      \} 

" ## added by OPAM user-setup for vim / base ## 93ee63e278bdfc07d1139a748ed3fff2 ## you can edit, but keep this line
let s:opam_share_dir = system("opam config var share")
let s:opam_share_dir = substitute(s:opam_share_dir, '[\r\n]*$', '', '')

let s:opam_configuration = {}

function! OpamConfOcpIndent()
  execute "set rtp^=" . s:opam_share_dir . "/ocp-indent/vim"
endfunction
let s:opam_configuration['ocp-indent'] = function('OpamConfOcpIndent')

function! OpamConfOcpIndex()
  execute "set rtp+=" . s:opam_share_dir . "/ocp-index/vim"
endfunction
let s:opam_configuration['ocp-index'] = function('OpamConfOcpIndex')

function! OpamConfMerlin()
  let l:dir = s:opam_share_dir . "/merlin/vim"
  execute "set rtp+=" . l:dir
endfunction
let s:opam_configuration['merlin'] = function('OpamConfMerlin')

let s:opam_packages = ["ocp-indent", "ocp-index", "merlin"]
let s:opam_check_cmdline = ["opam list --installed --short --safe --color=never"] + s:opam_packages
let s:opam_available_tools = split(system(join(s:opam_check_cmdline)))
for tool in s:opam_packages
  " Respect package order (merlin should be after ocp-index)
  if count(s:opam_available_tools, tool) > 0
    call s:opam_configuration[tool]()
  endif
endfor
" ## end of OPAM user-setup addition for vim / base ## keep this line
